//
//  Utils.h
//  wpccoderepo
//
//  Created by 汪鹏程 on 2021/9/9.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Utils : NSObject

+ (void)testLog;

@end

NS_ASSUME_NONNULL_END
